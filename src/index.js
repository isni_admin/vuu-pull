
import vuuPull from './pull.vue'

const Pull = {
    install (Vue, options) {
        Vue.component(vuuPull.name, vuuPull)
    }
}

// auto install
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
}

export default Pull