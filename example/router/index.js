import Vue from 'vue'
import Router from 'vue-router'
import index from '../page/index.vue'
import page1 from '../page/page-1.vue'
import page2 from '../page/page-2.vue'
import page3 from '../page/page-3.vue'
import page4 from '../page/page-4.vue'
import page5 from '../page/page-5.vue'
import page6 from '../page/page-6.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/page1',
      name: 'page1',
      component: page1
    },
    {
      path: '/page2',
      name: 'page2',
      component: page2
    },
    {
      path: '/page3',
      name: 'page3',
      component: page3
    },
    {
      path: '/page4',
      name: 'page4',
      component: page4
    },
    {
      path: '/page5',
      name: 'page5',
      component: page5
    },
    {
      path: '/page6',
      name: 'page6',
      component: page6
    }
  ]
})
