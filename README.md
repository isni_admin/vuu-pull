# Vuu-Pull@1.0.4
一个集成了下拉刷新、上拉加载、无限滚动加载的Vue组件。

## 在线示例
<img src="http://kabuwin.cn/vuu-pull/qrcode.png" style="display: block;" alt="qrcode">

[Demo - 点击demo切换到手机模式](http://kabuwin.cn/vuu-pull)

## 安装
```
 npm install vuu-pull --save

```

## 快速上手

``` vue

import vuuPull from 'vuu-pull'
vue.use(vuuPull)

<template>
   <vuu-pull ref="vuuPull"  :options="pullOptions" v-on:loadTop="loadTop"  v-on:loadBottom="loadBottom">
       <template v-for="(item , index) in paneList">
          <div>...</div>
       </template>
  </vuu-pull>
</template>
<script>
  let paneList = [... ]

  export default{
    data(){
      return {
        pullOptions: {
          isBottomRefresh:true,
          isTopRefresh:true
        }
      }
    },
    methods: {
      'loadBottom'(){

        setTimeout( ()=> {
          this.paneList.push({
            w1: '这是刷新的一条数据',
            w2: '太棒了！！请继续'
          })
          if (this.$refs.vuuPull.closeLoadBottom) {
            this.$refs.vuuPull.closeLoadBottom()
          }
        }, 1500)
      },
      'loadTop'(){
        setTimeout(()=> {
          this.paneList.unshift({
            w1: '这是下拉的一条数据',
            w2: '太棒了！！请继续下拉'
          })
          if (this.$refs.vuuPull.closeLoadTop) {
            this.$refs.vuuPull.closeLoadTop()
          }
        }, 1500)
      }
    }
  }
</script>

 ```
组件需要被包含在有效宽高的模块下，组件会默认占据父元素的百分之百高度。

[更多使用示例请参考Example的代码](https://gitee.com/begon/vuu-pull/tree/master/example)

 ## API文档

 ### props[options]
| 属性 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| topCloseElMove | 是否关闭对象在下拉移动 | Boolean | false |
| bottomCloseElMove | 是否关闭对象在上拉移动 | Boolean | false |
| isTopRefresh | 是否开启下拉刷新 | Boolean | false |
| isBottomRefresh | 是否开启上拉刷新 | Boolean | false |
| isTopBounce | 是否开启下拉弹性效果 | Boolean | false |
| isBottomBounce | 是否开启上拉弹性效果 | Boolean | false |
| isViewPullShadow | 是否开启阴影效果 | Boolean | false |
| pullType | 设置native开启仿原生下拉 | String | normal |
| slideResistance | 滑动的阻力 | Number | 2 |
| topTriggerHeight | 下拉触发刷新的有效距离 | Number | 50 |
| bottomTriggerHeight | 上拉触发刷新的有效距离 | Number | 50 |
| scrollTriggerValue | 设置滚动到底部多少触发刷新 | Number | 10 |
| topPull | 容器配置(文字，图标...) | Object | 默认配置 |
| bottomPull | 容器配置(文字，图标...) | Object | 默认配置 |

`topPull `和`bottomPull `可配置的选项和默认配置项的值
``` javascript
const DEFAULT_CONFIG = {
  pullWord: '下拉刷新', // 下拉时显示的文字
  triggerWord: '释放更新', // 下拉到触发距离时显示的文字
  loadingWord: '加载中...', // 加载中的文字
  pullIcon: '下拉图标', //
  loadingIcon: '加载图标', //
  nativePullIcon: '仿原生下拉图标' //

}

```

 ### events
| 事件名 | 说明 |
| --- | --- |
| loadTop | 触发下拉刷新的时候 |
| loadBottom | 触发上拉刷新的时候 |
| scrollBottom | 触发滚动刷新的时候 |
